'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const MarthaStaff = {
    "work_days": [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ],
    "count_services": 0,
    "total_sales": 0,
    "_id": "5e2c540044389327b8284b1c",
    "name": "Martha Rizzo",
    "telefono": "930723840",
    "instagram": "@rizzosmakeup",
    "__v": 0
}

var StaffSchema = Schema({
    name: String,
    telefono: String,
    instagram: String,
    work_days: Array,
    count_services: {
        type: Number, default: 0
    },
    total_sales: {
        type: Number, default: 0
    }
    

});

module.exports = mongoose.model('Staff', StaffSchema);

//client -->guarda documentos de este tipo y con estructura dentro de la colección
