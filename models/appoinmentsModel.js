'use strict'
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var Client = mongoose.model('Client');
var Service = mongoose.model('Service');
var StaffMember = mongoose.model('Staff');

var AppointmentSchema = Schema({
    client: {type: Schema.ObjectId, ref:'Client', default: "5e41e0d0dd10ec043421b07e"},
    services: [],
    staff_member: {type: Schema.ObjectId, ref:'Staff', default: "5e41e32c521c2d1f2ce825df"},
    date: {
        type:Date, default: Date.now
    },
});

module.exports = mongoose.model('Appointment', AppointmentSchema);
//Services -->guarda documentos de este tipo y con estructura dentro de la colección
