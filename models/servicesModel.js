'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Category = mongoose.model('Category');

var ServiceSchema = Schema({
    name: String,
    category: {
        type: Schema.ObjectId, 
        ref:'Category', 
        
        default: '5e4058eb93d5e92dc48461e7'},
    price: {
        type:Number, default:0
    },
    sales: {
        type:Number, default:0
    },
    total_sales: {
        type:Number, default:0
    },
    

});

module.exports = mongoose.model('Service', ServiceSchema);
//Services -->guarda documentos de este tipo y con estructura dentro de la colección
