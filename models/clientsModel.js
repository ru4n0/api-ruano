'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = Schema({
    name: String,
    telefono: String,
    instagram: String,
    last_visit: {
        type: Date, default: null
    },
    total_paid: {
        type: Number, default: 0
    }
    

});

module.exports = mongoose.model('Client', ClientSchema);
//client -->guarda documentos de este tipo y con estructura dentro de la colección
