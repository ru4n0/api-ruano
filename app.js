'use strict'

//modulos de node para crear servidor
var express = require('express');
var bodyParser = require('body-parser');

//Ejecutar express (http)
var app = express();
//cargar ficheros rutas
var article_routes = require('./routes/articleRoutes');
var client_routes = require('./routes/clientRoutes');
var category_routes = require('./routes/categoryRoutes');
var service_routes = require('./routes/serviceRoutes');
var staff_member_routes = require('./routes/staffRoutes');
var appointment_routes = require('./routes/appointmentRoutes');

//middleware
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//CORS PERMITIR LLAMADAS DESDE OTRO DOMINIO O IP
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


//Añadir prefijos a rutas - Cargar rutas
app.use('/api/articles/',article_routes)
app.use('/api/clients',client_routes)
app.use('/api/categories',category_routes)
app.use('/api/services',service_routes)
app.use('/api/staff_members',staff_member_routes)
app.use('/api/appointment', appointment_routes)

//exportar módulo (fichero actual)
module.exports = app;
