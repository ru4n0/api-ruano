'use strict'

var express = require('express');
var staffController = require('../controllers/staffController');

var router = express.Router();

var multipart = require('connect-multiparty');

//rutas utiles
router.post('/save', staffController.save);
router.get('/staff/:_id', staffController.getStaffMember);
router.put('/staff/:_id', staffController.update);
router.delete('/staff/:_id', staffController.delete);
router.get('/staff/:last?', staffController.getStaff);
router.get('/search/:search',staffController.search)
module.exports = router;