'use strict'

var express = require('express');
var ArticleController = require('../controllers/articleController');

var router = express.Router();

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: './upload/articles'});



//Rutas de prueba
router.get('/test', ArticleController.test);
router.post('/probando', ArticleController.datosCurso);

//rutas utiles
router.post('/save', ArticleController.save);
router.get('/article/:_id', ArticleController.getArticle);
router.put('/article/:_id', ArticleController.update);
router.delete('/article/:_id', ArticleController.delete);
router.post('/upload-image/:_id', md_upload, ArticleController.upload);
router.get('/get-image/:image',ArticleController.getImage);
router.get('/articles/:last?', ArticleController.getArticles);
router.get('/search/:search',ArticleController.search)
module.exports = router;