'use strict'

var express = require('express');
var appointmentController = require('../controllers/appointmentController');

var router = express.Router();

var multipart = require('connect-multiparty');

//rutas utiles
router.post('/save', appointmentController.save);
router.get('/appointment/:_id', appointmentController.getAppointment);
router.put('/appointment/:_id', appointmentController.update);
router.delete('/appointment/:_id', appointmentController.delete);
router.get('/appointment/:last?', appointmentController.getAppointments);
router.get('/pruebasAsyncAwait', async () => {
    async function primerLog(){
        console.log("1");
    }
    async function segundoLog(){
        console.log("2");
    }
    async function tercerLog(){
        console.log("3-0");
        setTimeout(() => {
            console.log("3")
        }, 3000)
        
    }
    async function cuartoLog(){
        console.log("4");
    }

    await new Promise((res, rej)=>{
        primerLog();
        res()
    }); 
    await new Promise((res, rej)=>{
        segundoLog();
        res()
    }); 
    await new Promise((res, rej)=>{
        tercerLog();
        res()
    }); 
    await new Promise((res, rej)=>{
        cuartoLog();
        res()
    });
})
module.exports = router;