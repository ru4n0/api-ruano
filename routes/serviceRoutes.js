'use strict'

var express = require('express');
var serviceController = require('../controllers/serviceController');

var router = express.Router();

var multipart = require('connect-multiparty');

//rutas utiles
router.post('/save', serviceController.save);
router.get('/service/:_id', serviceController.getService);
router.put('/service/:_id', serviceController.update);
router.delete('/service/:_id', serviceController.delete);
router.get('/service/:last?', serviceController.getServices);
router.get('/search/:search',serviceController.search)
module.exports = router;