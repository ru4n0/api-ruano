'use strict'

var express = require('express');
var categoryController = require('../controllers/categoryController');

var router = express.Router();

var multipart = require('connect-multiparty');

//rutas utiles
router.post('/save', categoryController.save);
router.get('/category/:_id', categoryController.getCategory);
router.put('/category/:_id', categoryController.update);
router.delete('/category/:_id', categoryController.delete);
router.get('/category/:last?', categoryController.getCategories);
router.get('/search/:search',categoryController.search)
module.exports = router;