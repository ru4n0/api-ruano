'use strict'

var express = require('express');
var clientController = require('../controllers/clientController');

var router = express.Router();

var multipart = require('connect-multiparty');

//rutas utiles
router.post('/save', clientController.save);
router.get('/client/:_id', clientController.getClient);
router.put('/client/:_id', clientController.update);
router.delete('/client/:_id', clientController.delete);
router.get('/client/:last?', clientController.getClients);
router.get('/search/:search',clientController.search)
module.exports = router;