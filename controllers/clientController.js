'use strict'

var validator = require('validator');
var Client = require('../models/clientsModel');

var fs = require('fs');
var path = require('path');

var controller = {

    save: (req, res) =>{
        //recoger parametros
        var params = req.body;
        
        //validar datos (validator)
        try {

            var validate_name = !validator.isEmpty(params.name);

        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Faltan datos por enviar.'
            });
        }
        if(validate_name){
            console.log('validación correcta');
            //crear el objeto a guardar
            var client = new Client(params);

              //Guardar articulo
            client.save((error, clientStored) =>{
                if(error || !clientStored){
                    return res.status(404).send({
                        status:'error',
                        message: 'El cliente no se ha guardado'
                    });
                }
                else{
                    //Devolver respuesta
                    return res.status(200).send({
                        status:'success',
                        client: clientStored
                    });

                }
            });
        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'Los datos no son validos.'
            });
        }
        
    },

    getClients: (req, res) =>{
        
        let last = req.params.last;
        console.log(req.params);
        let query = Client.find({});

        if(last || last != undefined){
            query.limit(5)
        }
        //Find
        query.sort('-_id').exec((error, clients) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Error al devolver los clientes.'
        
                });
            }

            if(!clients){
                return res.status(404).send({
                    status:'error',
                    message: 'No hay clientes.'
        
                });
            }

            return res.status(200).send({
                status:'succes',
                clients
            });
        });

        
    },

    getClient: (req, res) =>{

        let clientId = req.params._id;

        if(!clientId || clientId == null){
            return res.status(404).send({
                status:'error',
                message: 'No existe el cliente.'
    
            });    
        }

        //Comprobar que existe
        Client.findById(clientId, (error, client) => {
            if(error || !client){
                return res.status(404).send({
                    status:'error',
                    message: 'No existe el cliente.'
        
                });
            }

            return res.status(200).send({
                status:'success',
                client: client
    
            });
        });
        //Buscar el articulo

        //Devolver el articulo
    },

    update: (req, res) => {
        let clientId = req.params._id;

        //Recoger los datos que llegan por put
        let params = req.body;

        //Validar datos
        try {
            var validate_name = !validator.isEmpty(params.name);
        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Error en validación de datos a actualizar.'
            });
        }
        
        if(validate_name){

            //find and update
            Client.findOneAndUpdate({_id:clientId}, params, {new:true}, (error, clientUpdated)=>{
                //Devolver respuesta
                if(error){
                    return res.status(500).send({
                        status:'error',
                        message: 'Error al actualizar.'
                    });
                }

                if(!clientUpdated){
                    return res.status(404).send({
                        status:'error',
                        message: 'No existe el cliente.'
                    });
                }

                return res.status(200).send({
                    status:'success',
                    client: clientUpdated
                });
            });

        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'La validación no es correcta.'
            });
        }
    },
    delete: (req, res) => {

        let clientId = req.params._id;
        Client.findOneAndDelete({_id: clientId}, (error, clientRemoved) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Cliente no encontrado.'
                });
            }

            if(!clientRemoved){
                return res.status(404).send({
                    status:'error',
                    message: 'Cliente no encontrado.'
                });
            }

            return res.status(200).send({
                status:'success',
                client: clientRemoved
            });
        });
    },

    
    search: (req, res) => {

        var searchString = req.params.search;

        Client.find({
            "$or":[
                {"name": {"$regex": searchString, "$options": "i"}},
                {"instagram": {"$regex": searchString, "$options": "i"}},
                {"telefono": {"$regex": searchString, "$options": "i"}},
            ]
        })
        .sort([['name', 'descending']])
        .exec((err, clients) => {
            console.log(clients);
            if(err){
                return res.status(500).send({
                    status:'error', 
                    message:'error en la consulta.'
                });
            }

            if(!clients || clients.length <1){
                return res.status(404).send({
                    status:'error', 
                    message:'No se encontraron clientes relacionados.'
                });
            }

            return res.status(200).send({
                status:'succes', 
                clients
            });
        })
    }
}

module.exports = controller;