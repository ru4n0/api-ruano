'use strict'

var validator = require('validator');
var Staff = require('../models/staffModel');

const MarthaStaff = {
    "work_days": [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ],
    "count_services": 0,
    "total_sales": 0,
    "name": "Martha Rizzo",
    "telefono": "930723840",
    "instagram": "@rizzosmakeup"
}

var controller = {

    save: (req, res) =>{
        //recoger parametros
        var params = req.body;
        
        //validar datos (validator)
        try {

            var validate_name = !validator.isEmpty(params.name);

        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Faltan datos por enviar.'
            });
        }
        if(validate_name){
            console.log('validación correcta');
            //crear el objeto a guardar
            var staff = new Staff(MarthaStaff);

            //Asignar valores
            //staff.name = params.name;
            //staff.telefono = params.telefono;
            //staff.instagram = params.instagram;
            //staff.work_days = params.work_days;

            //Guardar articulo
            staff.save((error, staffStored) =>{
                if(error || !staffStored){
                    return res.status(404).send({
                        status:'error',
                        message: 'El staff no se ha guardado'
                    });
                }
                else{
                    //Devolver respuesta
                    return res.status(200).send({
                        status:'success',
                        staff: staffStored
                    });

                }
            });
        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'Los datos no son validos.'
            });
        }
        
    },

    getStaff: (req, res) =>{
        
        let last = req.params.last;
        console.log(req.params);
        let query = Staff.find({});

        if(last || last != undefined){
            query.limit(5)
        }
        //Find
        query.sort('-_id').exec((error, staff) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Error al devolver los staff.'
        
                });
            }

            if(!staff){
                return res.status(404).send({
                    status:'error',
                    message: 'No hay staff.'
        
                });
            }

            return res.status(200).send({
                status:'succes',
                staff
            });
        });

        
    },

    getStaffMember: (req, res) =>{

        let staffId = req.params._id;

        if(!staffId || staffId == null){
            return res.status(404).send({
                status:'error',
                message: 'No existe el staff member.'
    
            });    
        }

        //Comprobar que existe
        Staff.findById(staffId, (error, staff) => {
            if(error || !staff){
                return res.status(404).send({
                    status:'error',
                    message: 'No existe el staff member.'
        
                });
            }

            return res.status(200).send({
                status:'success',
                staff: staff
    
            });
        });
    },

    update: (req, res) => {
        let staffId = req.params._id;

        //Recoger los datos que llegan por put
        let params = req.body;

        //Validar datos
        try {
            var validate_name = !validator.isEmpty(params.name);
        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Error en validación de datos a actualizar.'
            });
        }
        
        if(validate_name){

            //find and update
            Staff.findOneAndUpdate({_id:staffId}, params, {new:true}, (error, staffUpdated)=>{
                //Devolver respuesta
                if(error){
                    return res.status(500).send({
                        status:'error',
                        message: 'Error al actualizar.'
                    });
                }

                if(!staffUpdated){
                    return res.status(404).send({
                        status:'error',
                        message: 'No existe el staff member.'
                    });
                }

                return res.status(200).send({
                    status:'success',
                    staff: staffUpdated
                });
            });

        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'La validación no es correcta.'
            });
        }
    },
    delete: (req, res) => {

        let staffId = req.params._id;
        Staff.findOneAndDelete({_id: staffId}, (error, staffRemoved) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Staff Member no encontrado.'
                });
            }

            if(!staffRemoved){
                return res.status(404).send({
                    status:'error',
                    message: 'Staff Member no encontrado.'
                });
            }

            return res.status(200).send({
                status:'success',
                staff: staffRemoved
            });
        });
    },

    
    search: (req, res) => {

        var searchString = req.params.search;

        Staff.find({
            "$or":[
                {"name": {"$regex": searchString, "$options": "i"}},
            ]
        })
        .sort([['name', 'descending']])
        .exec((err, staff) => {
            console.log(staff);
            if(err){
                return res.status(500).send({
                    status:'error', 
                    message:'error en la consulta.'
                });
            }

            if(!staff || staff.length <1){
                return res.status(404).send({
                    status:'error', 
                    message:'No se encontraron staff members relacionados.'
                });
            }

            return res.status(200).send({
                status:'success', 
                staff
            });
        })
    }
}

module.exports = controller;