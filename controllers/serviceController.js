'use strict'

var validator = require('validator');
var Service = require('../models/servicesModel');
var Category = require('../models/categoriesModel');

var fs = require('fs');
var path = require('path');

var controller = {

    save: (req, res) =>{
        //recoger parametros
        var params = req.body;
        
        //validar datos (validator)
        try {

            var validate_name = !validator.isEmpty(params.name);

        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Faltan datos por enviar.'
            });
        }
        if(validate_name){
            console.log('validación correcta');
            //crear el objeto a guardar
            var service = new Service(params);

            //Asignar valores
            //service = params;
            //service.name = params.name;
            //service.category = params.category;
            //service.price = params.price;
            

            //Guardar articulo
            service.save((error, serviceStored) =>{
                if(error || !serviceStored){
                    return res.status(404).send({
                        status:'error',
                        message: 'El servicio no se ha guardado.'
                    });
                }
                else{
                    //Devolver respuesta
                    return res.status(200).send({
                        status:'success',
                        service: serviceStored
                    });

                }
            });
        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'Los datos no son validos.'
            });
        }
        
    },

    getServices: (req, res) =>{
        
        let last = req.params.last;
        console.log(req.params);
        let query = Service.find({}).populate('category');

        if(last || last != undefined){
            query.limit(5)
        }
        //Find
        query.sort('-_id').exec((error, services) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Error al devolver los servicios.'
        
                });
            }

            if(!services){
                return res.status(404).send({
                    status:'error',
                    message: 'No hay servicios.'
        
                });
            }

            return res.status(200).send({
                status:'succes',
                services
            });
        });

        
    },

    getService: (req, res) =>{

        let serviceId = req.params._id;
        console.log(req.params);

        if(!serviceId || serviceId == null){
            return res.status(404).send({
                status:'error',
                message: 'No existe el servicio1.'
    
            });    
        }

        let query = Service.findById({_id: serviceId}).populate('category');

        //Comprobar que existe
        query.exec((error, service) => {
            if(error || !service){
                return res.status(404).send({
                    status:'error',
                    message: 'No existe el servicio.'
        
                });
            }

            return res.status(200).send({
                status:'success',
                service: service
    
            });
        });
        //Buscar el articulo

        //Devolver el articulo
    },

    update: (req, res) => {
        let serviceId = req.params._id;

        //Recoger los datos que llegan por put
        let params = req.body;

        //Validar datos
        try {
            var validate_name = !validator.isEmpty(params.name);
        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Error en validación de datos a actualizar.'
            });
        }
        
        if(validate_name){

            //find and update
            Service.findOneAndUpdate({_id:serviceId}, params, {new:true}, (error, serviceUpdated)=>{
                //Devolver respuesta
                if(error){
                    return res.status(500).send({
                        status:'error',
                        message: 'Error al actualizar.'
                    });
                }

                if(!serviceUpdated){
                    return res.status(404).send({
                        status:'error',
                        message: 'No existe el servicio.'
                    });
                }

                return res.status(200).send({
                    status:'success',
                    service: serviceUpdated
                });
            });

        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'La validación no es correcta.'
            });
        }
    },
    delete: (req, res) => {

        let serviceId = req.params._id;
        Service.findOneAndDelete({_id: serviceId}, (error, serviceRemoved) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Servicio no encontrado.'
                });
            }

            if(!serviceRemoved){
                return res.status(404).send({
                    status:'error',
                    message: 'Servicio no encontrado.'
                });
            }

            return res.status(200).send({
                status:'success',
                service: serviceRemoved
            });
        });
    },

    
    search: (req, res) => {

        var searchString = req.params.search;

        Service.find({
            "$or":[
                {"name": {"$regex": searchString, "$options": "i"}},
            ]
        })
        .sort([['name', 'descending']])
        .exec((err, services) => {
            console.log(services);
            if(err){
                return res.status(500).send({
                    status:'error', 
                    message:'error en la consulta.'
                });
            }

            if(!services || services.length <1){
                return res.status(404).send({
                    status:'error', 
                    message:'No se encontraron servicios relacionados.'
                });
            }

            return res.status(200).send({
                status:'succes', 
                services
            });
        })
    }
}

module.exports = controller;