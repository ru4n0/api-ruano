'use strict'

var validator = require('validator');
var Article = require('../models/articles');

var fs = require('fs');
var path = require('path');

var controller = {
    
    datosCurso: (req, res)=>{
        let variable = req.body.variable
        return res.status(200).send({
            curso: 'Master en Frameworkd',
            nombre: 'Luis Ruano',
            variable
        });
    },

    test:(req, res) =>{
        return res.status(200).send({
            message: 'Soy la accion teste de mi controlador de articulos'
        });
    },

    save: (req, res) =>{
        //recoger parametros
        var params = req.body;
        
        //validar datos (validator)
        try {

            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);

        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Faltan datos por enviar.'
            });
        }
        if(validate_title && validate_content){
            console.log('validación correcta');
            //crear el objeto a guardar
            var article = new Article();

            //Asignar valores
            article.title = params.title;
            article.content = params.content;
            article.image = null;

            //Guardar articulo
            article.save((error, articleStored) =>{
                if(error || !articleStored){
                    return res.status(404).send({
                        status:'error',
                        message: 'El articulo no se ha guardado'
                    });
                }
                else{
                    //Devolver respuesta
                    return res.status(200).send({
                        status:'success',
                        article: articleStored
                    });

                }
            });
        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'Los datos no son validos.'
            });
        }
        
    },

    getArticles: (req, res) =>{
        
        let last = req.params.last;
        console.log(req.params);
        let query = Article.find({});

        if(last || last != undefined){
            query.limit(5)
        }
        //Find
        query.sort('-_id').exec((error, articles) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Error al devolver los articulos.'
        
                });
            }

            if(!articles){
                return res.status(404).send({
                    status:'error',
                    message: 'No hay articulos'
        
                });
            }

            return res.status(200).send({
                status:'succes',
                articles
            });
        });

        
    },

    getArticle: (req, res) =>{

        let articleId = req.params._id;

        if(!articleId || articleId == null){
            return res.status(404).send({
                status:'error',
                message: 'No existe el articulo'
    
            });    
        }

        //Comprobar que existe
        Article.findById(articleId, (error, article) => {
            if(error || !article){
                return res.status(404).send({
                    status:'error',
                    message: 'No existe el articulo'
        
                });
            }

            return res.status(200).send({
                status:'success',
                article: article
    
            });
        });
        //Buscar el articulo

        //Devolver el articulo
    },

    update: (req, res) => {
        let articleId = req.params._id;

        //Recoger los datos que llegan por put
        let params = req.body;

        //Validar datos
        try {
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);
        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Error en validación de datos a actualizar'
            });
        }
        
        if(validate_title && validate_content){

            //find and update
            Article.findOneAndUpdate({_id:articleId}, params, {new:true}, (error, articleUpdated)=>{
                //Devolver respuesta
                if(error){
                    return res.status(500).send({
                        status:'error',
                        message: 'Error al actualizar'
                    });
                }

                if(!articleUpdated){
                    return res.status(404).send({
                        status:'error',
                        message: 'No existe el articulo'
                    });
                }

                return res.status(200).send({
                    status:'success',
                    article: articleUpdated
                });
            });

        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'La validación no es correcta.'
            });
        }
    },
    delete: (req, res) => {

        let articleId = req.params._id;
        Article.findOneAndDelete({_id: articleId}, (error, articleRemoved) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Articulo no encontrado.'
                });
            }

            if(!articleRemoved){
                return res.status(404).send({
                    status:'error',
                    message: 'Articulo no encontrado.'
                });
            }

            return res.status(200).send({
                status:'success',
                article: articleRemoved
            });
        });
    },

    upload: (req, res) => {
        //configurar el modulo connect multiparty router/article.js

        //Recoger archivo de la peticion
        var file_name = 'Imagen no subida...';

        if(!req.files){
            return res.status(404).send({
                status:'error',
                message: file_name
            });
        }

        //Recoger el nombre y la extensión del archivo
        var file_path = req.files.file0.path;
        var file_split = file_path.split('\\');
        var file_name = file_split[2];
        var split_extension = file_name.split('.');
        var file_ext = split_extension[split_extension.length-1];
        var extensiones_permitidas = ['png', 'jpeg', 'jpg', 'gif'];

        //Comprobar extensión (solo imagenes), si no es valido borrar el fichero
        if(extensiones_permitidas.indexOf(file_ext) == -1){
            
            fs.unlink(file_path, (err) => {

                return res.status(200).send({
                    status:'error',
                    message: "El tipo de archivo seleccionado no esta permitido.",
                });
            })
            
        }
        else{
            //Si todo es valido, buscar archivo y actualizarlo
        
            let params = req.body;

            let articleId = req.params._id;

            Article.findOneAndUpdate({_id: articleId}, {
                image: file_name
            }, {new:true}, (error, articleUpdated) => {

                if(error || articleUpdated == null){

                    fs.unlink(file_path, (err) => {

                        return res.status(200).send({
                            status:'error',
                            message: "Error al guardar la imagen.",
                        });
                    })
                }
                else{
                    return res.status(200).send({
                        status:'success',
                        message: "Archivo subido",
                        articleUpdated
                    });
                }
            });
        }
    },
    getImage: (req, res) => {
        var file = req.params.image;
        var path_file = './upload/articles/'+file;

        fs.exists(path_file, (exists) => {
            if(exists){
                return res.sendFile(path.resolve(path_file))
            }else{
                return res.status(404).send({
                    status:'error',
                    message: "Imagen no encontrada.",
                });
            }
        })

        
    },
    search: (req, res) => {

        var searchString = req.params.search;

        Article.find({
            "$or":[
                {"title": {"$regex": searchString, "$options": "i"}},
                {"content": {"$regex": searchString, "$options": "i"}},
            ]
        })
        .sort([['date', 'descending']])
        .exec((err, articles) => {
            console.log(articles);
            if(err){
                return res.status(500).send({
                    status:'error', 
                    message:'error en la consulta.'
                });
            }

            if(!articles || articles.length <1){
                return res.status(404).send({
                    status:'error', 
                    message:'No se encontraron articulos relacionados.'
                });
            }

            return res.status(200).send({
                status:'succes', 
                articles
            });
        })
    }
}

module.exports = controller;