'use strict'

var validator = require('validator');
var Category = require('../models/categoriesModel');

var fs = require('fs');
var path = require('path');

var controller = {

    save: (req, res) =>{
        //recoger parametros
        var params = req.body;
        
        //validar datos (validator)
        try {

            var validate_name = !validator.isEmpty(params.name);

        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Faltan datos por enviar.'
            });
        }
        if(validate_name){
            console.log('validación correcta');
            //crear el objeto a guardar
            var category = new Category();

            //Asignar valores
            category.name = params.name;
            category.description = params.description;

            //Guardar articulo
            category.save((error, categoryStored) =>{
                if(error || !categoryStored){
                    return res.status(404).send({
                        status:'error',
                        message: 'La categoría no se ha guardado'
                    });
                }
                else{
                    //Devolver respuesta
                    return res.status(200).send({
                        status:'success',
                        category: categoryStored
                    });

                }
            });
        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'Los datos no son validos.'
            });
        }
        
    },

    getCategories: (req, res) =>{
        
        let last = req.params.last;
        console.log(req.params);
        let query = Category.find({});

        if(last || last != undefined){
            query.limit(5)
        }
        //Find
        query.sort('-_id').exec((error, categories) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Error al devolver los categorías.'
        
                });
            }

            if(!categories){
                return res.status(404).send({
                    status:'error',
                    message: 'No hay categorías.'
        
                });
            }

            return res.status(200).send({
                status:'succes',
                categories
            });
        });

        
    },

    getCategory: (req, res) =>{

        let categoryId = req.params._id;

        if(!categoryId || categoryId == null){
            return res.status(404).send({
                status:'error',
                message: 'No existe La categoría.'
    
            });    
        }

        //Comprobar que existe
        Category.findById(categoryId, (error, category) => {
            if(error || !category){
                return res.status(404).send({
                    status:'error',
                    message: 'No existe La categoría.'
        
                });
            }

            return res.status(200).send({
                status:'success',
                category: category
    
            });
        });
        //Buscar el articulo

        //Devolver el articulo
    },

    update: (req, res) => {
        let categoryId = req.params._id;

        //Recoger los datos que llegan por put
        let params = req.body;

        //Validar datos
        try {
            var validate_name = !validator.isEmpty(params.name);
        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Error en validación de datos a actualizar.'
            });
        }
        
        if(validate_name){

            //find and update
            Category.findOneAndUpdate({_id:categoryId}, params, {new:true}, (error, categoryUpdated)=>{
                //Devolver respuesta
                if(error){
                    return res.status(500).send({
                        status:'error',
                        message: 'Error al actualizar.'
                    });
                }

                if(!categoryUpdated){
                    return res.status(404).send({
                        status:'error',
                        message: 'No existe La categoría.'
                    });
                }

                return res.status(200).send({
                    status:'success',
                    category: categoryUpdated
                });
            });

        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'La validación no es correcta.'
            });
        }
    },
    delete: (req, res) => {

        let categoryId = req.params._id;
        Category.findOneAndDelete({_id: categoryId}, (error, categoryRemoved) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'categorye no encontrado.'
                });
            }

            if(!categoryRemoved){
                return res.status(404).send({
                    status:'error',
                    message: 'categorye no encontrado.'
                });
            }

            return res.status(200).send({
                status:'success',
                category: categoryRemoved
            });
        });
    },

    
    search: (req, res) => {

        var searchString = req.params.search;

        Category.find({
            "$or":[
                {"name": {"$regex": searchString, "$options": "i"}},
                
            ]
        })
        .sort([['name', 'descending']])
        .exec((err, categories) => {
            console.log(categories);
            if(err){
                return res.status(500).send({
                    status:'error', 
                    message:'error en la consulta.'
                });
            }

            if(!categories || categories.length <1){
                return res.status(404).send({
                    status:'error', 
                    message:'No se encontraron categorías relacionados.'
                });
            }

            return res.status(200).send({
                status:'succes', 
                categories
            });
        })
    }
}

module.exports = controller;