'use strict'

var validator = require('validator');
var Appointment = require('../models/appoinmentsModel');
var Client = require('../models/clientsModel');
var Service = require('../models/servicesModel');
var Staff = require('../models/staffModel');

var controller = {

    save: (req, res) =>{
        //recoger parametros
        var params = req.body;
        
        //validar datos (validator)
        try {

            var validate_services = (params.services.length > 0);
            

        } 
        catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Faltan datos por enviar.'
            });
        }
        if(validate_services){
            console.log('validación correcta');
            //crear el objeto a guardar
            var appointment = new Appointment(params);

            //Asignar valores
            //appointment.name = params.name;
            //appointment.telefono = params.telefono;
            //appointment.instagram = params.instagram;
            //appointment.work_days = params.work_days;

            //Guardar articulo
            appointment.save((error, appointmentStored) =>{
                if(error || !appointmentStored){
                    return res.status(404).send({
                        status:'error',
                        message: 'El appointment no se ha guardado'
                    });
                }
                else{
                    //Devolver respuesta
                    return res.status(200).send({
                        status:'success',
                        appointment: appointmentStored
                    });

                }
            });
        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'Los datos no son validos.'
            });
        }
        
    },

    getAppointments: async (req, res)=>{
        let appoint = await Appointment.find({})
        .populate({path: 'client', select:'name'})
        .populate({path:'staff_member', select: 'name'}).exec((error, appointments) => {
            if(error || !appointments){
                return res.status(400).send({
                    status:'No existen reservas.'
                });
            }
            console.log(appointments);

            
    

        });

        console.log("Console log final");
    

        

       // appointments = await GetServicesAppointments(appointments)
            
        

        
    },

    getAppointment: (req, res) =>{

        let appointmentId = req.params._id;

        if(!appointmentId || appointmentId == null){
            return res.status(404).send({
                status:'error',
                message: 'No existe el appointment member.'
    
            });    
        }

        //Comprobar que existe
        Appointment.findById(appointmentId, (error, appointment) => {
            if(error || !appointment){
                return res.status(404).send({
                    status:'error',
                    message: 'No existe el appointment.'
        
                });
            }

            return res.status(200).send({
                status:'success',
                appointment: appointment
    
            });
        });
    },

    update: (req, res) => {
        let appointmentId = req.params._id;

        //Recoger los datos que llegan por put
        let params = req.body;

        //Validar datos
        try {
            var validate_services = (params.services.length > 0);
        } catch (error) {
            return res.status(200).send({
                status:'error',
                message: 'Error en validación de datos a actualizar.'
            });
        }
        
        if(validate_services){

            //find and update
            Appointment.findOneAndUpdate({_id:appointmentId}, params, {new:true}, (error, appointmentUpdated)=>{
                //Devolver respuesta
                if(error){
                    return res.status(500).send({
                        status:'error',
                        message: 'Error al actualizar.'
                    });
                }

                if(!appointmentUpdated){
                    return res.status(404).send({
                        status:'error',
                        message: 'No existe el appointment member.'
                    });
                }

                return res.status(200).send({
                    status:'success',
                    appointment: appointmentUpdated
                });
            });

        }
        else{
            return res.status(200).send({
                status:'error',
                message: 'La validación no es correcta.'
            });
        }
    },
    delete: (req, res) => {

        let appointmentId = req.params._id;
        Appointment.findOneAndDelete({_id: appointmentId}, (error, appointmentRemoved) => {
            if(error){
                return res.status(500).send({
                    status:'error',
                    message: 'Appointment no encontrado.'
                });
            }

            if(!appointmentRemoved){
                return res.status(404).send({
                    status:'error',
                    message: 'Appointment no encontrado.'
                });
            }

            return res.status(200).send({
                status:'success',
                appointment: appointmentRemoved
            });
        });
    },
}
async function GetAllAppointments(){
    //let query = 

    //return 
}
async function GetServicesAppointments(appointments, callback){

    appointments.map((app)=>{
            console.log("app map", app);
            app.services[0].info_service = Service.findById({_id: app.services[0].id_services}).exec((error, service) => {
                if(error || !service){
                    return {
                        status:'error',
                        message: 'No existe el servicio.'
                    };
                }
                console.log("Servicio info en map", service);
                return service
            });

            return app;
    });
    callback(appointments)
}

async function GetInfoService(serviceId){
    console.log("serviceId", serviceId);
    let query = ""

    let info_service = {}

        //Comprobar que existe
    info_service = await Service.findById({_id: serviceId}).exec((error, service) => {
        if(error || !service){
            return {
                status:'error',
                message: 'No existe el servicio.'
            };
        }
        info_service = service
        console.log("Service", service);
        return info_service

        
    });

    return info_service;
}

module.exports = controller;